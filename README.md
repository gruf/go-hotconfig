# go-hotconfig

A configuration parser designed with hot-reloads (i.e. reloading during runtime) in mind. Supports customizable parsing from CLI args, environment vars and configuration file.