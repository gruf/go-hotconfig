package env

import (
	"reflect"
	"strings"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-hotconfig/internal"
	"golang.org/x/exp/slices"
)

// Flags provides helpful methods for a slice of flags.
type Flags []Flag

// Validate will check over receiving Flags for valid flag keys and conflicts.
func (set *FlagSet) Validate() error {
	flags := make(map[string]*Flag, len(set.Flags))
	for i := 0; i < len(set.Flags); i++ {
		switch flag := &(set.Flags[i]); {
		// Flag contains no matchable key
		case flag.Key == "":
			return &InvalidFlagError{Msg: "empty key"}

		// Flag key must not contain any illegal chars
		case strings.ContainsAny(flag.Key, internal.IllegalChars):
			return &InvalidFlagError{Key: flag.Key, Msg: "contains illegal char"}

		// Flag keys must be unique
		case flags[flag.Key] != nil:
			return &InvalidFlagError{Key: flag.Key, Msg: "has key conflict"}

		// Flag reflect type, kind and value must be set
		case flag.kind == reflect.Invalid || flag.rtype == nil || !flag.rvalue.IsValid():
			return &InvalidFlagError{Key: flag.Key, Msg: "uninitialized flag"}

		// New flag
		default:
			flags[flag.Key] = flag
		}
	}
	return nil
}

// Get will fetch the flag with matching env key, or return nil.
func (f Flags) Get(key string) *Flag {
	for i := 0; i < len(f); i++ {
		if flag := &f[i]; flag.Key == key {
			return flag
		}
	}
	return nil
}

// Sort will alphabetically sort receiving Flags.
func (f Flags) Sort() {
	slices.SortFunc(f, func(i, j Flag) bool {
		if i.Key != "" && j.Key != "" {
			return i.Key < j.Key
		}
		return false
	})
}

// AppendFormat will append a new-line separated string representation of Flags to b.
func (f Flags) AppendFormat(b []byte) []byte {
	for i := 0; i < len(f); i++ {
		b = append(b, f[i].Key...)
		b = append(b, '=')
		if v := f[i].Value; v != nil {
			b = append(b, *v...)
		}
		b = append(b, '\n')
	}
	return b
}

// String will return a new-line separated string representation of Flags.
func (f Flags) String() string {
	buf := byteutil.Buffer{B: make([]byte, 0, len(f)*32)}
	buf.B = f.AppendFormat(buf.B)
	return buf.String()
}

// generateFlags contains the main logic of GenerateFlags(), setup to allow recursion.
func generateFlags(rvalue reflect.Value, fields []reflect.StructField) []Flag {
	var flags []Flag

outer:
	for i := 0; i < len(fields); i++ {
		var (
			// configured CLI flag
			flag Flag

			// does struct field require flag
			hasFlag bool
		)

		if tag := fields[i].Tag.Get(EnvTag); internal.IsValidTag(tag) {
			// Set flag's key
			flag.Key = tag
			hasFlag = true
		}

		var (
			t = fields[i].Type
			k = t.Kind()
			c = 0
		)

		for k == reflect.Pointer {
			// Dereference ptr
			t = t.Elem()
			k = t.Kind()
			c++
		}

		// Get struct field value
		vfield := rvalue.Field(i)

		if !hasFlag && k == reflect.Struct {
			for i := 0; i < c; i++ {
				if vfield.IsNil() {
					// ignore nil structs
					continue outer
				}

				// Dereference pointer
				vfield = vfield.Elem()
			}

			// Recursively generate flags for structs
			subfields := internal.StructFieldsForType(fields[i].Type)
			flags = append(flags, generateFlags(vfield, subfields)...)
			continue
		}

		// Set flag's type+value
		flag.kind = k
		flag.rtype = fields[i].Type
		flag.rvalue = vfield

		// Append flag to slice
		flags = append(flags, flag)
	}

	return flags
}
