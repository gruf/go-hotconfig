package env

import (
	"fmt"
	"reflect"
)

// Env var struct tag.
const EnvTag = "env"

type Flag struct {
	// Key is the env flag key name.
	Key string

	// Value contains determined value string for this
	// flag, populated by the .Parse() function.
	Value *string

	// rtype is the reflected type of this Flag's destination.
	rtype reflect.Type

	// kind is the fully dereferenced reflect kind of this Flag's destination type.
	kind reflect.Kind

	// value is the reflected destination value location for this Flag.
	rvalue reflect.Value
}

// NewFlag ...
func NewFlag(dst interface{}, key string, value string) Flag {
	// Determine type information
	rtype := reflect.TypeOf(dst)

	if rtype == nil {
		panic("invalid dst")
	} else if rtype.Kind() != reflect.Pointer {
		panic("dst must be pointer")
	}

	var (
		t = rtype
		k = t.Kind()
	)

	for k == reflect.Pointer {
		// Deref pointer
		t = t.Elem()
		k = t.Kind()
	}

	return Flag{
		Key:    key,
		rtype:  rtype,
		kind:   k,
		rvalue: reflect.ValueOf(dst),
	}
}

// InvalidFlagError encapsulates error information regarding
// an invalid flag when calling env.Flags{}.Validate().
type InvalidFlagError struct {
	Key string
	Msg string
}

func (err *InvalidFlagError) Error() string {
	if err.Key == "" {
		// no flag key provided, simply show message
		return "invalid flag: " + err.Msg
	}
	return fmt.Sprintf("invalid flag key: %q %s", err.Key, err.Msg)
}
