package env

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// Parse ...
// will parse value strings from env vars slice, storing in the Decoder before call to Decode().
func (set *FlagSet) Parse(env []string) error {
	// Validate currently provided flags
	if err := set.Validate(); err != nil {
		return err
	}

	for i := 0; i < len(env); i++ {
		env := env[i]

		// Look for key-value separator
		idx := strings.IndexByte(env, '=')
		if idx < 0 {
			// invalid
			continue
		}

		// Look for env flag with key, set value if so
		if flag := set.Flags.Get(env[:idx]); flag != nil {
			value := env[idx+1:]
			flag.Value = &value

			// Attempt to unmarshal flag values into destination
			if err := set.Unmarshal(flag.rvalue, flag.rtype, value); err != nil {
				return fmt.Errorf("error unmarshaling %q value: %w", flag.Key, err)
			}
		}
	}

	return nil
}

// ParseFile ...
func (set *FlagSet) ParseFile(src io.Reader) error {
	// Validate currently provided flags
	if err := set.Validate(); err != nil {
		return err
	}

	// Wrap file in buffered reader
	br := bufio.NewReader(src)

	for {
		// Read next line from file
		line, err := br.ReadString('\n')
		if err != nil {
			// EOF is not an issue
			if err == io.EOF {
				break
			}

			// Unrecoverable
			return err
		}

		// Trim newline
		line = line[:len(line)-1]

		// Check for a valid key-value pair line
		idx := strings.IndexByte(line, '=')
		if idx < 0 {
			return fmt.Errorf("invalid line in .env file: %q", line)
		}

		// Split into key-val
		key := line[:idx]
		val := line[idx+1:]

		if len(val) >= 2 && // nocollapse
			(quotedBy(val, '\'') || quotedBy(val, '"')) {
			// trim quote chars from value
			val = val[1 : len(val)-1]
		}

		// Look for env flag with key, set value if so
		if flag := set.Flags.Get(key); flag != nil {
			flag.Value = &val

			// Attempt to unmarshal flag values into destination
			if err := set.Unmarshal(flag.rvalue, flag.rtype, val); err != nil {
				return fmt.Errorf("error unmarshaling %q value: %w", flag.Key, err)
			}
		}
	}

	return nil
}

// quotedBy will check if str is quoted by char c, accounting for delimiting slashes '\'.
func quotedBy(str string, c byte) bool {
	// Check this str is started/ended by 'c'
	if str[0] != c || str[len(str)-1] != c {
		return false
	}

	var delim bool

	for i := len(str) - 2; i >= 0; i-- {
		// Break on non-delim
		if str[i] != '\\' {
			break
		}

		// Toggle value
		delim = !delim
	}

	return !delim
}
