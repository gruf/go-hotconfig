package env

import (
	"reflect"
	"strconv"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-hotconfig/internal"
)

type FlagSet struct {
	// Flags should contain possible keys to be searched for when parsing env vars.
	Flags Flags

	// Unmarshal should unmarshal possible environment value into 'dst'.
	Unmarshal func(dst reflect.Value, typ reflect.Type, src string) error
}

func (set *FlagSet) Bool(key string, value bool) *bool {
	ptr := new(bool)
	set.BoolVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) BoolVar(ptr *bool, key string, value bool) {
	var vstr string
	if value {
		vstr = strconv.FormatBool(value)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) Int(key string, value int) *int {
	ptr := new(int)
	set.IntVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) IntVar(ptr *int, key string, value int) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatInt(int64(value), 10)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) Int64(key string, value int64) *int64 {
	ptr := new(int64)
	set.Int64Var(ptr, key, value)
	return ptr
}

func (set *FlagSet) Int64Var(ptr *int64, key string, value int64) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatInt(value, 10)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) Uint(key string, value uint) *uint {
	ptr := new(uint)
	set.UintVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) UintVar(ptr *uint, key string, value uint) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatUint(uint64(value), 10)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) Uint64(key string, value uint64) *uint64 {
	ptr := new(uint64)
	set.Uint64Var(ptr, key, value)
	return ptr
}

func (set *FlagSet) Uint64Var(ptr *uint64, key string, value uint64) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatUint(value, 10)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) Float(key string, value float64) *float64 {
	ptr := new(float64)
	set.FloatVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) FloatVar(ptr *float64, key string, value float64) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatFloat(value, 'f', -1, 64)
	}
	set.Var(ptr, key, vstr)
}

func (set *FlagSet) String(key string, value string) *string {
	ptr := new(string)
	set.StringVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) StringVar(ptr *string, key string, value string) {
	set.Var(ptr, key, value)
}

func (set *FlagSet) Bytes(key string, value []byte) *[]byte {
	ptr := new([]byte)
	set.BytesVar(ptr, key, value)
	return ptr
}

func (set *FlagSet) BytesVar(ptr *[]byte, key string, value []byte) {
	set.Var(ptr, key, byteutil.B2S(value))
}

func (set *FlagSet) Var(dst interface{}, key, value string) {
	flag := NewFlag(dst, key, value)
	set.Flags = append(set.Flags, flag)
}

func (set *FlagSet) StructFields(dst interface{}) {
	rtype := reflect.TypeOf(dst)

	if rtype.Kind() != reflect.Pointer {
		panic("dst must be struct pointer")
	}

	rvalue := reflect.ValueOf(dst)

	for rtype.Kind() == reflect.Pointer {
		// We need a non-nil dst
		if rvalue.IsNil() {
			panic("dst must be non-nil")
		}

		// Dereference pointer
		rtype = rtype.Elem()
		rvalue = rvalue.Elem()
	}

	if rtype.Kind() != reflect.Struct {
		panic("dst must be struct pointer")
	}

	// Generate fields for reflected struct type
	fields := internal.StructFieldsForType(rtype)
	flags := generateFlags(rvalue, fields)

	// Append new struct flags to FlagSet
	set.Flags = append(set.Flags, flags...)
}
