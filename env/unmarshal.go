package env

import (
	"reflect"

	"codeberg.org/gruf/go-strdecode"
)

// Unmarshaler returns a default Decoder unmarshaling func implementation based on given strdecode.Decoder instance.
func Unmarshaler(dec *strdecode.Decoder) func(dst reflect.Value, typ reflect.Type, src string) error {
	return dec.UnmarshalInto
}
