package cli

import (
	"fmt"
	"reflect"

	"codeberg.org/gruf/go-strdecode"
)

// Unmarshaler returns a default Decoder unmarshaling func implementation based on given strdecode.Decoder instance.
func Unmarshaler(dec *strdecode.Decoder) func(dst reflect.Value, typ reflect.Type, src []string) error {
	return func(dst reflect.Value, typ reflect.Type, src []string) error {
		switch len(src) {
		case 0:
			for typ.Kind() == reflect.Pointer {
				// Deref pointer
				typ = typ.Elem()
				dst = dst.Elem()
			}

			// This MUST be a boolean type
			if typ.Kind() != reflect.Bool {
				return fmt.Errorf("invalid arg count for %q", typ.String())
			}

			// Update bool value
			dst.SetBool(true)
			return nil

		case 1:
			// Unmarshal into dst using single value
			return dec.UnmarshalInto(dst, typ, src[0])

		default:
			for typ.Kind() == reflect.Pointer {
				// Deref pointer
				typ = typ.Elem()
				dst = dst.Elem()
			}

			// This MUST be a slice type
			if dst.Kind() != reflect.Slice {
				return fmt.Errorf("invalid arg acount for %q", typ.String())
			}

			// Get slice element type
			telem := dst.Type().Elem()

			// Reslice starting at 0
			next := dst.Slice(0, 0)

			for i := 0; i < len(src); i++ {
				// Allocate new element
				elem := reflect.New(telem)
				elem = elem.Elem()

				// Unmarshal element value for source element
				if err := dec.UnmarshalInto(elem, typ, src[i]); err != nil {
					return err
				}

				// Append next element value
				next = reflect.Append(next, elem)
			}

			// Set return slice
			dst.Set(next)
			return nil
		}
	}
}
