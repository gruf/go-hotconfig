package cli

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"codeberg.org/gruf/go-byteutil"
)

// Parse ...
// will parse value strings from CLI arguments slice, storing in the Decoder before call to Decode(). Returns unrecognized arguments.
func (set *FlagSet) Parse(args []string) (unknown []string, err error) {
	// Validate currently provided flags
	if err := set.Validate(); err != nil {
		return nil, err
	}

	for i := 0; i < len(args); i++ {
		var (
			// Argument at current index
			arg = args[i]

			// Match flag idx for arg
			idx int

			// Current flag's matching string value (if set)
			value *string
		)

		if len(arg) == 0 || len(arg) == 1 || arg[0] != '-' {
			// Unrecognizable argument, add to unknown
			unknown = append(unknown, arg)
			continue
		}

		// Check if this has compound value (e.g. --verbose=true)
		if idx := strings.IndexByte(arg, '='); idx > 0 {
			v := arg[idx+1:]
			value = &v
			arg = arg[:idx]
		}

		if arg[1] == '-' {
			// Prefix='--' --> parse long flag
			idx = set.Flags.GetLong(arg[2:])
		} else {
			// Prefix='-' --> parse short flag
			idx = set.Flags.GetShort(arg[1:])
		}

		if idx < 0 {
			// Unrecognizable argument, add to unknown
			unknown = append(unknown, arg)
			continue
		}

		if value == nil {
			if set.Flags[idx].kind == reflect.Bool {
				// Only booleans allow no value,
				// so set a sentinel empty value.
				set.Flags[idx].Values = []string{}
				continue
			}

			if i == len(args)-1 {
				// Value expected and none was found
				return nil, fmt.Errorf("flag %q expects a value", arg)
			}

			// Jump to next
			i++

			// Grab next as value
			value = &args[i]
		}

		// Append next arg value to existing values slice
		set.Flags[idx].Values = append(set.Flags[idx].Values, *value)
	}

loop:
	for i := 0; i < len(set.Flags); i++ {
		switch {
		// Values were provided for flag
		case set.Flags[i].Values != nil:

		// Flag is required but no values
		case set.Flags[i].Required:
			return nil, fmt.Errorf("flag %q is required", set.Flags[i].Name())

		// Flag is unset but has default
		case set.Flags[i].Default != "":
			set.Flags[i].Values = []string{set.Flags[i].Default}

		// Unset flag, ignore
		default:
			continue loop
		}

		// Attempt to unmarshal flag values into destination
		if err := set.Unmarshal(set.Flags[i].rvalue, set.Flags[i].rtype, set.Flags[i].Values); err != nil {
			return nil, fmt.Errorf("error unmarshaling %q value: %w", set.Flags[i].Name(), err)
		}
	}

	return unknown, nil
}

// ParseStrict will perform .Parse() but return error in the case that unknown arguments are returned.
func (set *FlagSet) ParseStrict(args []string) error {
	unknown, err := set.Parse(args)
	if len(unknown) > 0 {
		return errors.New("unknown arguments: \"" + quoteslice(unknown) + "\"")
	}
	return err
}

// quoteslice will join a slice of strings quoting each.
func quoteslice(s []string) string {
	var buf byteutil.Buffer
	for i := 0; i < len(s); i++ {
		buf.B = strconv.AppendQuote(buf.B, s[i])
		buf.B = append(buf.B, ',')
	}
	buf.Truncate(1)
	return buf.String()
}
