package cli

import (
	"fmt"
	"reflect"
	"strings"
)

const (
	// Short flag name struct tag.
	ShortNameTag = "short"

	// Long flag name struct tag.
	LongNameTag = "long"

	// Usage flag struct tag.
	UsageTag = "usage"

	// Required flag struct tag.
	RequiredTag = "required"

	// Default value struct tag.
	DefaultTag = "default"
)

type Flag struct {
	// Short is the short-form flag name, parsed from CLI flags prefixed by '-'.
	Short string

	// Long is the long-form flag name, parsed from CLI flags prefixed by '--'.
	Long string

	// Usage is the description for this CLI flag, used when generating usage string.
	Usage string

	// Required specifies whether this flag is required, if so returning error.
	Required bool

	// Default is the default value for this CLI flag, used when no value provided.
	Default string

	// Values contains raw value strings populated during parse stage.
	Values []string

	// rtype is the reflected type of this Flag's destination.
	rtype reflect.Type

	// kind is the fully dereferenced reflect kind of this Flag's destination type.
	kind reflect.Kind

	// value is the reflected destination value location for this Flag.
	rvalue reflect.Value
}

// NewFlag ...
func NewFlag(dst interface{}, short, long string, value string, usage string) Flag {
	// Determine type information
	rtype := reflect.TypeOf(dst)

	if rtype == nil {
		panic("invalid dst")
	} else if rtype.Kind() != reflect.Pointer {
		panic("dst must be pointer")
	}

	var (
		t = rtype
		k = t.Kind()
	)

	for k == reflect.Pointer {
		// Deref pointer
		t = t.Elem()
		k = t.Kind()
	}

	return Flag{
		Short:   short,
		Long:    long,
		Usage:   usage,
		Default: value,
		rtype:   rtype,
		kind:    k,
		rvalue:  reflect.ValueOf(dst),
	}
}

// AppendUsage will append (new-line terminated) usage information for flag to b.
func (f Flag) AppendUsage(b []byte) []byte {
	b = append(b, ' ')

	if f.Short != "" {
		// Append short-flag
		b = append(b, " -"...)
		b = append(b, f.Short...)
	}

	if f.Long != "" {
		// Append long-flag
		b = append(b, " --"...)
		b = append(b, f.Long...)
	}

	if f.kind != reflect.Bool {
		// Append type information
		b = append(b, ' ')
		b = append(b, f.kind.String()...)
	}

	if f.Usage != "" {
		// Append usage information
		b = append(b, "\n    \t"...)
		b = append(b, strings.ReplaceAll(
			// Replace new-lines with
			// prefixed new-lines.
			f.Usage,
			"\n",
			"\n    \t",
		)...)
	}

	if f.Default != "" {
		// Append default information
		b = append(b, " (default "...)
		b = append(b, f.Default...)
		b = append(b, ')')
	}

	b = append(b, '\n')
	return b
}

// Name ...
func (f Flag) Name() string {
	switch {
	case f.Long != "":
		return "--" + f.Long
	case f.Short != "":
		return "-" + f.Short
	default:
		return ""
	}
}

// InvalidFlagError encapsulates error information regarding
// an invalid flag when calling cli.Flags{}.Validate().
type InvalidFlagError struct {
	Name string
	Type string
	Msg  string
}

func (err *InvalidFlagError) Error() string {
	switch {
	case err.Name == "":
		return "invalid flag: " + err.Msg
	case err.Type == "":
		return fmt.Sprintf("invalid flag: %q %s", err.Name, err.Msg)
	default:
		return fmt.Sprintf("invalid flag %s name: %q %s", err.Type, err.Name, err.Msg)
	}
}
