package cli

import (
	"reflect"
	"strings"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-hotconfig/internal"
	"golang.org/x/exp/slices"
)

// Flags provides helpful methods for a slice of flags.
type Flags []Flag

// Validate will check over receiving set Flags for valid flag short/long names and conflicts.
func (set *FlagSet) Validate() error {
	flags := make(map[string]*Flag, len(set.Flags))
	for i := 0; i < len(set.Flags); i++ {
		switch flag := &(set.Flags[i]); {
		// Flag contains no matchable name
		case flag.Short == "" && flag.Long == "":
			return &InvalidFlagError{Msg: "no short / long name"}

		// Flag short name must be on a single char
		case len(flag.Short) > 1:
			return &InvalidFlagError{Name: flag.Short, Type: "short", Msg: "length > 1"}

		// Flag short / long name must not begin with '-'
		case strings.HasPrefix(flag.Short, "-"):
			return &InvalidFlagError{Name: flag.Short, Type: "short", Msg: "has '-' prefix"}
		case strings.HasPrefix(flag.Long, "-"):
			return &InvalidFlagError{Name: flag.Long, Type: "long", Msg: "has '-' prefix"}

		// Flag short / long name must not contain any illegal chars
		case strings.ContainsAny(flag.Short, internal.IllegalChars):
			return &InvalidFlagError{Name: flag.Short, Type: "short", Msg: "contains illegal char"}
		case strings.ContainsAny(flag.Long, internal.IllegalChars):
			return &InvalidFlagError{Name: flag.Long, Type: "long", Msg: "contains illegal char"}

		// Flag short / long names must be unique
		case flags[flag.Short] != nil:
			return &InvalidFlagError{Name: flag.Short, Type: "short", Msg: "has name conflict"}
		case flags[flag.Long] != nil:
			return &InvalidFlagError{Name: flag.Long, Type: "long", Msg: "has name conflict"}

		// Flag reflect type, kind and value must be set
		case flag.kind == reflect.Invalid || flag.rtype == nil || !flag.rvalue.IsValid():
			return &InvalidFlagError{Name: flag.Name(), Msg: "uninitialized flag"}

		// New flag
		default:
			flags[flag.Short] = flag
			flags[flag.Long] = flag
		}
	}
	return nil
}

// GetShort will fetch the flag index with matching short name, or -1.
func (f Flags) GetShort(name string) int {
	for i := 0; i < len(f); i++ {
		if f[i].Short == name {
			return i
		}
	}
	return -1
}

// GetLong will fetch the flag index with matching long name, or -1.
func (f Flags) GetLong(name string) int {
	for i := 0; i < len(f); i++ {
		if f[i].Long == name {
			return i
		}
	}
	return -1
}

// Sort will alphabetically sort receiving Flags.
func (f Flags) Sort() {
	slices.SortFunc(f, func(i, j Flag) bool {
		if i.Short != "" && j.Short != "" {
			return i.Short < j.Short
		}
		if i.Long != "" && j.Long != "" {
			return i.Long < j.Long
		}
		return false
	})
}

// AppendUsage will call .AppendUsage() for each contained Flag and return accumulated bytes.
func (f Flags) AppendUsage(b []byte) []byte {
	for i := 0; i < len(f); i++ {
		b = f[i].AppendUsage(b)
	}
	return b
}

// Usage will generate an accumulated usage string for all of the contained Flags.
func (f Flags) Usage() string {
	buf := byteutil.Buffer{B: make([]byte, 0, len(f)*32)}
	buf.B = f.AppendUsage(buf.B)
	return buf.String()
}

// generateFlags contains the main logic of GenerateFlags(), setup to allow recursion.
func generateFlags(rvalue reflect.Value, fields []reflect.StructField) []Flag {
	var flags []Flag

outer:
	for i := 0; i < len(fields); i++ {
		var (
			// configured CLI flag
			flag Flag

			// does struct field require flag
			hasFlag bool
		)

		if tag := fields[i].Tag.Get(ShortNameTag); internal.IsValidTag(tag) {
			// Set flag's short-name
			flag.Short = tag
			hasFlag = true
		}

		if tag := fields[i].Tag.Get(LongNameTag); internal.IsValidTag(tag) {
			// Set flag's long-name
			flag.Long = tag
			hasFlag = true
		}

		if tag := fields[i].Tag.Get(UsageTag); internal.IsValidTag(tag) {
			// Set flag's usage information
			flag.Usage = tag
		}

		if tag := fields[i].Tag.Get(DefaultTag); internal.IsValidTag(tag) {
			// Set flag's default value
			flag.Default = tag
		}

		if tag := fields[i].Tag.Get(RequiredTag); internal.IsValidTag(tag) {
			// Set flag as required
			flag.Required = true
		}

		var (
			t = fields[i].Type
			k = t.Kind()
			c = 0
		)

		for k == reflect.Pointer {
			// Dereference ptr
			t = t.Elem()
			k = t.Kind()
			c++
		}

		// Get struct field value
		vfield := rvalue.Field(i)

		if !hasFlag && k == reflect.Struct {
			for i := 0; i < c; i++ {
				if vfield.IsNil() {
					// ignore nil structs
					continue outer
				}

				// Dereference pointer
				vfield = vfield.Elem()
			}

			// Recursively generate flags for structs
			subfields := internal.StructFieldsForType(fields[i].Type)
			flags = append(flags, generateFlags(vfield, subfields)...)
			continue
		}

		// Set flag's type+value
		flag.kind = k
		flag.rtype = fields[i].Type
		flag.rvalue = vfield

		// Append flag to slice
		flags = append(flags, flag)
	}

	return flags
}
