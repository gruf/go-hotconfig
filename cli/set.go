package cli

import (
	"reflect"
	"strconv"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-hotconfig/internal"
)

type FlagSet struct {
	// Flags should contain possible flags to be searched for when parsing CLI args.
	Flags Flags

	// Unmarshal should unmarshal possible CLI argument value slice into 'dst'. Note
	// that for singular values len = 1, if flag was provided with no value len = 0.
	Unmarshal func(dst reflect.Value, typ reflect.Type, src []string) error
}

func (set *FlagSet) Bool(short, long string, value bool, usage string, required bool) *bool {
	ptr := new(bool)
	set.BoolVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) BoolVar(ptr *bool, short, long string, value bool, usage string, required bool) {
	var vstr string
	if value {
		vstr = strconv.FormatBool(value)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) Int(short, long string, value int, usage string, required bool) *int {
	ptr := new(int)
	set.IntVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) IntVar(ptr *int, short, long string, value int, usage string, required bool) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatInt(int64(value), 10)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) Int64(short, long string, value int64, usage string, required bool) *int64 {
	ptr := new(int64)
	set.Int64Var(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) Int64Var(ptr *int64, short, long string, value int64, usage string, required bool) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatInt(value, 10)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) Uint(short, long string, value uint, usage string, required bool) *uint {
	ptr := new(uint)
	set.UintVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) UintVar(ptr *uint, short, long string, value uint, usage string, required bool) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatUint(uint64(value), 10)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) Uint64(short, long string, value uint64, usage string, required bool) *uint64 {
	ptr := new(uint64)
	set.Uint64Var(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) Uint64Var(ptr *uint64, short, long string, value uint64, usage string, required bool) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatUint(value, 10)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) Float(short, long string, value float64, usage string, required bool) *float64 {
	ptr := new(float64)
	set.FloatVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) FloatVar(ptr *float64, short, long string, value float64, usage string, required bool) {
	var vstr string
	if value != 0 {
		vstr = strconv.FormatFloat(value, 'f', -1, 64)
	}
	set.Var(ptr, short, long, vstr, usage, required)
}

func (set *FlagSet) String(short, long string, value string, usage string, required bool) *string {
	ptr := new(string)
	set.StringVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) StringVar(ptr *string, short, long string, value string, usage string, required bool) {
	set.Var(ptr, short, long, value, usage, required)
}

func (set *FlagSet) Bytes(short, long string, value []byte, usage string, required bool) *[]byte {
	ptr := new([]byte)
	set.BytesVar(ptr, short, long, value, usage, required)
	return ptr
}

func (set *FlagSet) BytesVar(ptr *[]byte, short, long string, value []byte, usage string, required bool) {
	set.Var(ptr, short, long, byteutil.B2S(value), usage, required)
}

func (set *FlagSet) Var(dst interface{}, short, long, value, usage string, required bool) {
	flag := NewFlag(dst, short, long, value, usage)
	flag.Required = required
	set.Flags = append(set.Flags, flag)
}

func (set *FlagSet) StructFields(dst interface{}) {
	rtype := reflect.TypeOf(dst)

	if rtype.Kind() != reflect.Pointer {
		panic("dst must be struct pointer")
	}

	rvalue := reflect.ValueOf(dst)

	for rtype.Kind() == reflect.Pointer {
		// We need a non-nil dst
		if rvalue.IsNil() {
			panic("dst must be non-nil")
		}

		// Dereference pointer
		rtype = rtype.Elem()
		rvalue = rvalue.Elem()
	}

	if rtype.Kind() != reflect.Struct {
		panic("dst must be struct pointer")
	}

	// Generate fields for reflected type
	fields := internal.StructFieldsForType(rtype)
	flags := generateFlags(rvalue, fields)

	// Append new struct flags to FlagSet
	set.Flags = append(set.Flags, flags...)
}
