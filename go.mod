module codeberg.org/gruf/go-hotconfig

go 1.18

require (
	codeberg.org/gruf/go-byteutil v1.0.1
	codeberg.org/gruf/go-strdecode v0.1.0
	golang.org/x/exp v0.0.0-20220518171630-0b5c67f07fdf
)

require codeberg.org/gruf/go-bytesize v0.2.0 // indirect
