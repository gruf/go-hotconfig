package hotconfig

import (
	"fmt"
	"os"
	"reflect"
	"sync"

	"codeberg.org/gruf/go-hotconfig/cli"
	"codeberg.org/gruf/go-hotconfig/env"
	"codeberg.org/gruf/go-strdecode"
)

type Config struct {
	// FlagsCLI allows specifying extra CLI flags ontop of those
	// generated from your values type. These will be returned to
	// you with raw string values from .ParseCLI(). An example use
	// case here is setting config file location, usage printing, etc.
	CLI cli.FlagSet

	// FlagsEnv allows specifying extra env flags ontop of those
	// generated from your values type. These will be returned to
	// you with raw string value from .ParseEnv(). An example use
	// case here is setting config file location.
	Env env.FlagSet

	// DecodeFile allows specifying a custom file decoder, where 'dst' interface{}
	// is your configuration values object and src is the file opened from path
	// passed to ConfigState.ReadFile().
	//
	// If nil the default of env.FileDecoder(...) will be used.
	DecodeFile func(dst interface{}, src *os.File) error
}

type ConfigState[Values any] struct {
	config Config     // runtime decoders for CLI,env,file
	values Values     // underlying stored values type
	mutex  sync.Mutex // protects access to values
}

func New[Values any](cfg Config) *ConfigState[Values] {
	var (
		// dec provides a shared decoder instance
		// that can be used across default values
		// of cfg.{Unmarshal*,DecodeFile}
		dec *strdecode.Decoder

		// decAlloc performs lazy init of 'dec'
		decAlloc = func() {
			if dec == nil {
				// decoder not yet allocated
				dec = &strdecode.Decoder{}
			}
		}

		// state is the returned ConfigState
		state = &ConfigState[Values]{config: cfg}
	)

	// Check this is a valid generic parameter type
	if reflect.TypeOf(state.values).Kind() != reflect.Struct {
		panic("only supports struct-type generic parameters")
	}

	if state.config.CLI.Unmarshal == nil {
		decAlloc()

		// Set default CLI flag decoder
		state.config.CLI.Unmarshal = cli.Unmarshaler(dec)
	}

	if state.config.Env.Unmarshal == nil {
		decAlloc()

		// Set default env value decoder
		state.config.Env.Unmarshal = env.Unmarshaler(dec)
	}

	if state.config.DecodeFile == nil {
		// Set default file decoder
		state.config.DecodeFile = func(_ interface{}, src *os.File) error {
			return state.config.Env.ParseFile(src)
		}
	}

	// Add values struct fields to CLI/env flags
	state.config.CLI.StructFields(&state.values)
	state.config.Env.StructFields(&state.values)
	state.config.CLI.Flags.Sort()
	state.config.Env.Flags.Sort()

	return state
}

// Config provides safe access to ConfigState's config values.
func (st *ConfigState[Values]) Config(fn func(*Values)) {
	st.mutex.Lock()
	defer st.mutex.Unlock()
	fn(&st.values)
}

// ParseCLI will parse CLI arguments and safely load them into the ConfigState's config values, returning unrecognized arguments.
func (st *ConfigState[Values]) ParseCLI(args []string) (unknown []string, err error) {
	st.mutex.Lock()
	defer st.mutex.Unlock()
	return st.config.CLI.Parse(args)
}

// ParseCLIStrict will perform .ParseCLI() but return error in the case that unknown arguments are returned.
func (st *ConfigState[Values]) ParseCLIStrict(args []string) error {
	st.mutex.Lock()
	defer st.mutex.Unlock()
	return st.config.CLI.ParseStrict(args)
}

// ParseEnv will parse environment variables and safely load them into the ConfigState's config values.
func (st *ConfigState[Values]) ParseEnv(env []string) error {
	st.mutex.Lock()
	defer st.mutex.Unlock()
	return st.config.Env.Parse(env)
}

// ReadFile will parse file at given path and safely load variables into ConfigState's config values.
func (st *ConfigState[Values]) ReadFile(path string) error {
	// Acquire ConfigState lock
	st.mutex.Lock()
	defer st.mutex.Unlock()

	// Open config file at path
	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("error opening config file: %w", err)
	}

	// Ensure closed
	defer file.Close()

	// Pass to user-provided config decoder
	return st.config.DecodeFile(&st.values, file)
}

// CLI provides access to the ConfigState's underlying cli.FlagSet. NOT CONCURRENCY SAFE.
func (st *ConfigState[Values]) CLI() *cli.FlagSet {
	return &st.config.CLI
}

// Env provides access to the ConfigState's underlying env.FlagSet. NOT CONCURRENCY SAFE.
func (st *ConfigState[Values]) Env() *env.FlagSet {
	return &st.config.Env
}
